<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Mahasiswa extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_mahasiswa','mhs');
        $this->methods['index_get']['limit'] = 500;
        $this->methods['index_delete']['limit'] = 500;
    }

    public function index_get(){
        $id         = $this->get('id');
        $keyword    = $this->get('keyword');

        if($id===null and $keyword===null){
            $mahasiswa = $this->mhs->getMahasiswa();
        }elseif($id != null){
            $mahasiswa = $this->mhs->getMahasiswa($id);
        }elseif($keyword != null){
            $mahasiswa = $this->mhs->getMahasiswaSearch($keyword);
        }
       

        if($mahasiswa){
            $this->set_response([
                'status'    => true,
                'data'      => $mahasiswa,
            ], REST_Controller::HTTP_OK); 
        }else{
            $this->set_response([
                'status'    => false,
                'message'   => "ID not found",
            ], REST_Controller::HTTP_NOT_FOUND); 
        }
    }

    public function index_delete(){
        $id =  $this->delete('id');
        if($id === null){
            $this->set_response([
                'status'    => false,
                'message'   => "Provide an id !",
            ], REST_Controller::HTTP_BAD_REQUEST); 
        }else{
            if($this->mhs->deleteMahasiswa($id) > 0){
                $this->set_response([
                    'status'    => true,
                    'id'        => $id,
                    'message'   => 'Deleted',
                ], REST_Controller::HTTP_OK); 
            }else{
                $this->set_response([
                    'status'    => false,
                    'message'   => "ID not found",
                ], REST_Controller::HTTP_BAD_REQUEST); 
            }
        }
    }

    public function index_post(){
        $data = [
            'nrp'       => $this->post('nrp'),
            'nama'      => $this->post('nama'),
            'email'     => $this->post('email'),
            'jurusan'   => $this->post('jurusan')
        ];

        if($this->mhs->createMahasiswa($data) > 0){
            $this->set_response([
                'status'    => true,
                'message'   => 'tambah sukses',
            ], REST_Controller::HTTP_CREATED); 
        }else{
            $this->set_response([
                'status'    => false,
                'message'   => "gagal simpan",
            ], REST_Controller::HTTP_BAD_REQUEST); 
        }
    }

    public function index_put(){

        $id     = $this->put('id');
        $data   = [
            'nrp'       => $this->put('nrp'),
            'nama'      => $this->put('nama'),
            'email'     => $this->put('email'),
            'jurusan'   => $this->put('jurusan')
        ];

        if($this->mhs->updateMahasiswa($data, $id) > 0){
            $this->set_response([
                'status'    => true,
                'message'   => 'update sukses',
            ], REST_Controller::HTTP_OK); 
        }else{
            $this->set_response([
                'status'    => false,
                'message'   => "gagal update",
            ], REST_Controller::HTTP_BAD_REQUEST); 
        }

    }

}